jQuery(document).ready(function($){
    $('a').not('.showModal').click(function(){
        var href = $(this).attr('href');

        if(href.startsWith('#') && href !== '#') {
            $('html, body').animate({scrollTop:$(href).offset().top}, 500);
            return false;
        }
    });

    $('.ajaxForm').submit(function(e){
        e.preventDefault();

        var $form = $(this);

        $('.message', $form).remove();
        $('.has-error', $form).removeClass('has-error');

        $.post('send.php', $form.serialize(), function(json){
            if(json.success) {
                $form.trigger('reset');

                hideModal('#' + $('.modal.show').eq(0).attr('id'));
                showModal('#successModal');
            } else {
                for(var key in json.errors) {
                    $('input[name="' + key + '"]', $form).addClass('has-error');
                    $('input[name="' + key + '"]', $form).after('<div class="message message--error">' + json.errors[key] + '</div>');
                }
            }
        }, 'json').fail(function(err){
            console.log(err);
        });
    });

    $('.playReview').click(function(){
       stopVideo($('#reviews .review__video'));
       playVideo($(this).closest('.video'));
    });

    $('.video').click(function(e){
        if(!$(e.target).hasClass('playReview')) stopVideo($(this));
    });

    $('.showModal').click(function(e){
        e.preventDefault();

        var modalId = $(this).attr('href');

        if(!modalId) return;

        showModal($(modalId));
    });

    $('.closeModal').click(function(e){
        e.preventDefault();

        hideModal('#' + $(this).closest('.modal').attr('id'));
    });

    $('#modalBg').click(function(){
        hideModal('#' + $('.modal.show').attr('id'));
    });

    function stopVideo($elem) {
        $elem.each(function(){
           $(this).removeClass('playing');
           $('video', this).get(0).pause();
        });
    }

    function playVideo($elem) {
        $elem.addClass('playing');
        $('video', $elem).get(0).play();
    }

    function showModal(modal) {
        $('#modalBg').addClass('show');
        $('body').addClass('modalOpen');
        $(modal).addClass('show');
    }

    function hideModal(modal) {
        $('#modalBg').removeClass('show');
        $('body').removeClass('modalOpen');
        $(modal).removeClass('show');
    }
});