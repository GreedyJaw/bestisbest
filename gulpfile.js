const gulp = require('gulp'),
	  autoprefixer = require('gulp-autoprefixer'),
	  sass = require('gulp-sass'),
	  jade = require('gulp-jade'),
	  clean = require('gulp-clean'),
	  cleanCss = require('gulp-clean-css'),
	  minify = require('gulp-minify'),
	  sourcemaps = require('gulp-sourcemaps'),
	  browserSync = require('browser-sync').create();

gulp.task('default', (done) =>
  gulp.series('watch', 'server')(done)
);

gulp.task('watch', (done) => {
    browserSync.init({
        server: {
            baseDir: "build"
        },
		online: true,
		tunnel: "my-test"
    });

    gulp.watch('dist', gulp.series('scripts:build', 'css:build', 'jade:build'));
    gulp.watch('build/*.html').on('change', browserSync.reload);
});

gulp.task('build', (done) => 
	gulp.series('clean', 'scripts:build', 'img:build', 'vids:build', 'fonts:build', 'assets:build', 'css:build', 'jade:build')(done)
);

gulp.task('clean', (done) => 
	gulp.src('build', {allowEmpty: true})
		.pipe(clean())
);

gulp.task('scripts:build', (done) => 
	gulp.src('dist/js/*.js')
		.pipe(gulp.dest('build/js'))
);

gulp.task('vids:build', (done) =>
	gulp.src('dist/vids/*.*')
		.pipe(gulp.dest('build/vids'))
);

gulp.task('img:build', (done) => 
	gulp.src('dist/img/**/*.*')
		.pipe(gulp.dest('build/img'))
);

gulp.task('fonts:build', (done) => 
	gulp.src('dist/fonts/**/*.*')
		.pipe(gulp.dest('build/fonts'))
);

gulp.task('assets:build', (done) =>
	gulp.src('dist/libs/**/*.*')
		.pipe(gulp.dest('build/libs'))
);

gulp.task('css:build', (done) => 
	gulp.src('dist/css/main.scss')
		.pipe(sass())
		.pipe(autoprefixer({
			cascade: false
		}))
		// .pipe(sourcemaps.init())
		// .pipe(cleanCss())
		// .pipe(sourcemaps.write('../maps'))
		.pipe(gulp.dest('build/css'))
);

gulp.task('jade:build', (done) => 
	gulp.src('dist/*.jade')
		.pipe(jade({
			pretty: true
		}))
		.pipe(gulp.dest('build'))
);